# Program for predicting survival rate of patient diagnosed with HCC
Java program predicting survival rate of patients diagnosed with HCC after one year based on 27 patient attributes

## Prerequisites
This program has been build under Java 11

therefore to run the program it is required that Java 11 has been installed

## Getting started
These instructions will let you run the program on your local machine

## requirements
To predict the survival rate of patients diagnosed with HCC, 27 values of attributes of patients diagnosed with HCC are needed to perform a prediction. The 27 attributes and their corresponding values needed to run the program can be found in the [example ARFF ](https://bitbucket.org/Hans96/wekaapi/src/master/example%20data/example.arff) defined  by @ATTRIBUTE The 27 attributes required for prediction can be entered in two ways:

1. Entering A multi batch ARFF file
2. Entering data via a single string text line

### entering via ARFF file
Running a prediction for multiple patients can be done via a ARFF file. The ARFF describes a list of instances sharing a set of attributes. The program makes use of 27 attributes mentioned in the link above. It is essential that these attributes correspond with the attributes used to run the program.It is therefore strongly adviced to copy the attribute tags ( defined by and starting with : @ATTRIBUTE) from the example ARFF from the [example ARFF ](https://bitbucket.org/Hans96/wekaapi/src/master/example%20data/example.arff) file into your own ARFF file to avoid errors. or you can replace the instances (defined after the @data mark) with your own instances having the exact same properties (single quotes, 27 attributes and first letter Capitalized).

### Entering via single text line
Another option is entering an instance (patient) via a single textline, the value of attributes need to be seperated by commas encapsulated by quotes for example:

"'No','No','No','No','No','No','Yes','No','Yes','No','No','Yes','Yes','No','No','No','Young age','Active','None',3,318,0,6.4,0.64,5,1.9,1,?"

Mind the single quotes and capitals for the nominal attributes.

## Running the program
This program is a command line program. The program and the desired input needs to be entered via the terminal

Go [here](https://bitbucket.org/Hans96/wekaapi/src/master/build/libs/) and download cli-demo-1.0-SNAPSHOT-all.jar, this is the commandline program that makes the actual prediction

Follow the next steps to run the program by entering a arff file:

1. Open a terminal
2. Go to the directory where the program is located
3. type java -jar cli-demo-1.0-SNAPSHOT-all.jar -f <Path/to/arff/file>

To run the program by entering a single text line, follow the next steps:

1. Open a terminal
2. Go to the directory where the program is located
3. type java -jar cli-demo-1.0-SNAPSHOT-all.jar -a <"Text line of 27 attributes seperated by commas">
4. For more help enter after the decleration of the program -h instead of -f or -a for more help.

## Output
The program outputs summary statistics of the predicted values in the terminal when a file is entered. The actual predicted values are stored in a textfile called result.txt

When an error occurs within the data values, the occurred errors will be writen to a text file called log.txt
