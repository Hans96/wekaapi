package nl.bioinf.hzijlstra.readmissionclassifier;

import org.apache.commons.cli.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Program that parses command line options being passed to programs
 * it further prints helps messages detailing the options that are available
 * for the user to enter in the command line
 */

public class ApacheCliOptionsProvider{
    private final String filePath = "app_data/stringInput.arff";

    private static final String HELP = "help";
    private static final String ATTRIBUTES = "attributes";
    private static final String FILE = "file";

    private final String[] clArguments;
    private CommandLine commandLine;
    private static Options options;

    /**
     * @param args Arguments provided by the user
     */

    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * Checks if help is requested by the user
     * @return boolean to confirm request
     */

    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * Creates options what kind of arguments the user can enter the program
     * Options that are being build is a help to tell the user how to run the program
     * A file option to let the user enter a ARFF file
     * A attribute option to let the user enter a single string seperated by commas, between quotes
     */

    private void buildOptions() {
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option attributeOption = new Option("a", ATTRIBUTES, true, "A single string of 27 values of matching attributes where each attribute is separated by commas and the complete string is capsulised by quotes");
        Option fileOption = new Option("f", FILE,true,"A ARFF type file containing attribute set predicting survival of HCC patients");
        options.addOption(helpOption);
        options.addOption(attributeOption);
        options.addOption(fileOption);
    }

    /**
     * Processes commandline arguments provided by the user
     * if the commandline option is a file a fileParser instance is created
     * to confirm if the file validates.
     * If the commandline argument is a string instance
     * the instance is written to a predefined ARFF file and validation occurs by file validating
     * After validation the line is removed from the file.
     * If the commandline option equals a call to help a help message is printed
     */

    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(options, this.clArguments);
            if (commandLine.hasOption(FILE)) {
                try {
                    String pathFile = this.commandLine.getOptionValue(FILE);
                    FileParser fileParser = new FileParser(pathFile);
                    fileParser.start();
                }catch (Exception e) {
                    System.err.println("The file you entered could not been found");
                }

            } else if (commandLine.hasOption(ATTRIBUTES)) {
                try {
                    String instance = this.commandLine.getOptionValue(ATTRIBUTES);
                    addLineToFile(instance);
                    FileParser fileParser = new FileParser(this.filePath);
                    fileParser.start();
                    removeLine(instance);
                } catch (Exception e) {
                    System.out.println("Something went wrong make sure you enter the correct options");
                    printHelp();
                }
            } else if (commandLine.hasOption(HELP)) {
                printHelp();
            }

        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Creates help method that prints help message to user
     */

    public static void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Help option Survival rate predicter", options);
    }

    /**
     * adds a new line to predefined arff file
     * @param instance line to be added
     */

    private void addLineToFile(String instance) {
        try {
            FileWriter lineWriter = new FileWriter(this.filePath,true);
            lineWriter.write(instance);
            lineWriter.close();

        } catch (IOException e) {
            System.out.println("Unable to write to file");
        }

    }

    /**
     * removes a line from predefined arff file
     * @param lineContent line to be removed from arff file
     */

    private void removeLine(String lineContent) {
        try {
            File file = new File(this.filePath);
            List<String> out = Files.lines(file.toPath())
                    .filter(line -> !line.contains(lineContent))
                    .collect(Collectors.toList());
            Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            System.out.println("Unable to remove line from file");
        }
    }


}
