package nl.bioinf.hzijlstra.readmissionclassifier;


import java.io.*;
import java.util.*;


/**
 *The FileParser program parses arff files.
 * it checks if arff type file meet specific attribute and file requirements
 * to run a weka program.
 * @author Hans Zijlstra
 */
public class FileParser {
    private final String file;

    /**
     * constructor for class fileParser
     * @param file contains file path to arff file
     */

    public FileParser(String file) {
        this.file = file;
    }

    /**
     * Method executes methods defined in class to perform checks on arff File
     * if no errors occur while parsing the file
     * The weka api will be run to perform calculation with the arff file data
     */

    public void start() {
        try {
            System.out.println("Starting program");
            System.out.println("Checking extension");
            if (checkExtension(this.file)) {
                System.out.println("Extension valid");
                List<String> userInput = getAttributesFromFile(this.file);
                List<String> expectedAttributes = getAttributesFromFile("app_data/stringInput.arff");
                System.out.println("Checking attributes");
                if (compareAttributes(userInput, expectedAttributes)) {
                    System.out.println("Attributes correct!");
                    if (checkDataLines(storeData()).isEmpty()) {
                        System.out.println("No errors in data");
                        WekaRunner wekaRunner = new WekaRunner(this.file);
                        wekaRunner.start();
                    } else {
                        File logFile = new File("logFile.txt");
                        if (logFile.isFile()) {
                            logFile.delete();
                            createNewFile(checkDataLines(storeData()), logFile);
                        } else {
                            createNewFile(checkDataLines(storeData()), logFile);
                        }
                        System.out.println("Incorrect values found in data");
                        System.out.println("Writing incorrect values to logfile");
                        System.out.println("See logfile.txt in the log directory to see ocurring errors ");
                        System.out.println("Aborting session");
                    }
                } else {
                    System.err.println("The provided file does not equal the attributes of the template file, make sure they are identical");
                }
            } else {
                System.out.println("The file you entered is not an arff file");
            }
        }catch (Exception e) {
            System.out.println("The file you entered could not be found");
            System.out.println("Aborting session");
        }
    }

    /**
     *Method that stores lines in arff file that start with "@"
     * @return Arraylist containing lines
     * @throws FileNotFoundException when file not found
     */

    private ArrayList<String> storeData() throws FileNotFoundException {
        try {
            ArrayList<String> data = new ArrayList<String>();
            FileReader reader = new FileReader(this.file);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String sCurrentLine;
            while ((sCurrentLine = bufferedReader.readLine()) != null) {
                if (!sCurrentLine.startsWith("@") && !sCurrentLine.startsWith("\n")) {
                    data.add(sCurrentLine);
                }
            }
            return data;
        } catch (Exception e) {
            throw new FileNotFoundException("The file you entered has not been found");
        }
    }

    /**
     * method that checks if data meets the requirments defined in arff template
     * @param data contains data values o
     * @return boolean to confirm validity of data
     */

    private ArrayList<String> checkDataLines(ArrayList<String> data) throws IOException {
        ArrayList<String> log = new ArrayList<>();
        int lineNumber = 30;
        for (Object obj : data) {
            lineNumber += 1;
            String[] line = obj.toString().split(",");
            if (line.length == 28) {
                int i = 0;
                while (i < line.length) {
                    if (i < 16 && !line[i].matches("'Yes'|'No'|\\?")) {
                        log.add("line " + lineNumber + ", attribute " + i + " is not equal to 'Yes', 'No' or ? but equals " + line[i] + "\n");
                    } else if (i == 16 && !line[i].matches("'Young age'|'Middle age'|'Old age'|\\?")) {
                        log.add("line " + lineNumber + ", attribute " + i + " is not equal to 'Young age','Middle age','Old age' or ? but equals " + line[i]  + "\n");
                    } else if (i == 17 && !line[i].matches("'Active'|'restricted'|'Ambulatory'|'Selfcare'|'Disabled'|\\?")) {
                        log.add("line " + lineNumber + ", attribute " + i + " is not equal to 'Active','restricted','Ambulatory','Selfcare','Disabled' or ? but equals " + line[i]  + "\n");
                    } else if (i == 18 && !line[i].matches("'None'|'Mild'|'Moderate/Severe'|\\?")) {
                        log.add("line " + lineNumber + ", attribute " + i + " is not equal to 'None','Mild','Moderate','Severe' or ? but equals " + line[i]  + "\n");
                    } else if (i > 18 && i < 27  && !line[i].matches("\\?") && !isNumeric(line[i])) {
                        log.add("line " + lineNumber + ", attribute " + i + " is not a valid number or ? but equals " + line[i]  + "\n");
                    }
                    i++;
                }
            } else {
                log.add("You did not enter 27 attributes on line " + lineNumber);
            }
        }
        return log;
    }

    /**
     *Method that stores attributes in the arff file.
     *checks if line start with @attribute and stores these accordingly
     * @param pathFile pathfile to arff file
     * @return arraylist containing attributes
     */


    private List<String> getAttributesFromFile(String pathFile) throws FileNotFoundException {
        try {
            List<String> attributes = new ArrayList<>();
            FileReader reader = new FileReader(pathFile);
            BufferedReader bufferedReader = new BufferedReader(reader);
            String sCurrentLine;
            while ((sCurrentLine = bufferedReader.readLine()) != null) {
                if (sCurrentLine.startsWith("@attribute")) {
                    attributes.add(sCurrentLine);
                }
            }
            return attributes;
        } catch (Exception e) {
            throw new FileNotFoundException();

        }
    }

    /**
     * Compares if attributes specified in arff file by user
     * is equal to the predefined required attributes
     * @param userAttributes List of attributes from user
     * @param expectedAttributes List of predefined attributes
     * @return boolean to confirm validity of comparing
     */

    private boolean compareAttributes(List<String> userAttributes, List<String> expectedAttributes) {
        return userAttributes.equals(expectedAttributes);

    }

    /**
     * Checks if the extension of the file entered by the user is arff
     * @param pathFile path to user his arff file
     * @return boolean to confirm validity of extension
     */

    private static boolean checkExtension(String pathFile) {
        return pathFile.endsWith("arff");
    }

    /**
     * Method that checks if string is a numeric value
     * @param str string to be parsed to double
     * @return boolean to confirm if string can be parsed to double
     */

    private static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Creates a new file and writes occurring errors to to a file
     *
     * @param data to be written to file
     * @param newFile File where data needs to be written to
     * @throws IOException when file cannot been written to
     */

    private void createNewFile(ArrayList data, File newFile) throws IOException {
        try {
            FileWriter writer = new FileWriter(newFile);
            for(Object str: data) {
                writer.write(str.toString());
            }
            writer.close();
    }catch (IOException e) {
            System.out.println("Could not write to file");
        }

        }


}




