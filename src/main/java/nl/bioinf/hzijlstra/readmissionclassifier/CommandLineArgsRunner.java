package nl.bioinf.hzijlstra.readmissionclassifier;

import org.apache.commons.cli.UnrecognizedOptionException;

import java.util.Arrays;

/**
 * Main class to start
 */

public final class CommandLineArgsRunner {

    /**
     * private constructor because this class is not supposed to be instantiated.
     */
    CommandLineArgsRunner() {
    }

    /**
     * @param args the command line arguments
     */

    public static void main(final String[] args) {
        try {
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);
            if(op.helpRequested()) {
                op.printHelp();
            }

        }catch (IllegalArgumentException e) {
            System.out.println("The option you entered is illegal");
            System.out.println("These are the valid options");
            ApacheCliOptionsProvider.printHelp();
        }



    }
}