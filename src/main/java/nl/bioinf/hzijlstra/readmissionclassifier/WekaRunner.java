package nl.bioinf.hzijlstra.readmissionclassifier;

import weka.classifiers.Evaluation;
import weka.classifiers.lazy.IBk;
import weka.classifiers.meta.CostSensitiveClassifier;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Normalize;

import java.io.*;
import java.util.Random;

/**
 * Program that makes use of the wekaapi and uses a predefined model
 * to predict if patients diagnosed with HCC will still be alive after one year
 */

public class WekaRunner {
    private final String unknownFile;
    private final String modelFile = "app_data/ibkmodel.model";

    /**
     * @param unknownFile file with unpredicted survival status for patient instances
     */

    public WekaRunner(String unknownFile) {
        this.unknownFile = unknownFile;
    }

    /**
     * Start methods defined in WekaRunner class
     * Runs prediction of unknown survival status of HCC patients
     * provided by the user
     */

    public void start() {
        try {
            if (!unknownFile.endsWith("stringInput.arff")) {
                IBk fromFile = loadClassifier();
                Instances unknownInstances = loadArff(this.unknownFile);
                Instances normalizedInstances = performNormalization(unknownInstances);
                File resultFile = new File("result/result.txt");
                if (checkExistenceFile(resultFile)) {
                    resultFile.delete();
                    createNewFile(classifyNewInstance(fromFile, normalizedInstances));
                    getStatistics(classifyNewInstance(fromFile, normalizedInstances));
                } else {
                    createNewFile(classifyNewInstance(fromFile, unknownInstances));
                }
            } else {
                IBk fromFile = loadClassifier();
                Instances unknownInstances = loadArff(this.unknownFile);
                Instances stringInstances = classifyNewInstance(fromFile, unknownInstances);
                createNewFile(stringInstances);
            }

            System.out.println("Writing outcome to result file");
            System.out.println("All done, see result.txt for the predicted results");

        } catch (Exception e) {
            System.out.println("Something went wrong");
            System.out.println(e.getMessage());
            System.out.println("Quiting program");
        }
    }

    /**
     * Classifies new instances provided by the user prints the predicted result
     * @param ibk
     * @param unknownInstances
     * @throws Exception
     * @return
     */

    public Instances classifyNewInstance(IBk ibk, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = ibk.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        return labeled;
    }

    /**

     * @return desirialized IBK model from predefined model
     * @throws Exception if model could not be deserialized
     */

    private IBk loadClassifier() throws Exception {
        // deserialize model
        return (IBk) weka.core.SerializationHelper.read(modelFile);
    }

    /**
     * Loads provided ARFF file and sets class argument to the last attribute in the file.
     * @param datafile
     * @return loaded instances from user provided data
     * @throws Exception if file could not be read
     */

    private Instances loadArff(String datafile) throws Exception {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            //calls Normalize method and returns normalized instances
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }

    /**
     *performs min max normalization on numeric values
     * @param data instances to be normalized
     * @return filtered data instances
     * @throws Exception when data can not be normalized
     */

    private Instances performNormalization(Instances data) throws Exception {
        Normalize normalize = new Normalize();
        normalize.setInputFormat(data);
        return Filter.useFilter(data, normalize);
    }

    private void createNewFile(Instances instances) throws IOException {
        try {
            File newFile = new File("result.txt");
            FileWriter writer = new FileWriter(newFile);
            writer.write(instances.toString());
            writer.close();

        }catch (IOException e) {
            System.out.println("Could not write to file");
        }

    }

    /**
     * Checks if file exists
     * @param file to be checked for existence
     * @return boolean for validation existence
     */

    private boolean checkExistenceFile(File file) {
        return file.isFile();
    }

    /**
     * prints the statistics of classified instances provided by the user
     * @param dataSet classified instances to calculate statistics
     * @throws Exception when statistics cannot be calculated
     */

    private void getStatistics(Instances dataSet) throws Exception {
        Evaluation eval = new Evaluation(dataSet);
        Random rand = new Random(1);
        int folds = 10;
        eval.crossValidateModel(loadClassifier(), dataSet, folds, rand);
        System.out.println("Here are the statistics of your data set");
        System.out.println(eval.toSummaryString());
        System.out.println(eval.toMatrixString());

    }


}

